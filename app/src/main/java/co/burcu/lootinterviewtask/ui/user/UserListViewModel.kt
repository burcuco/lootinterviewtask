package co.burcu.lootinterviewtask.ui.user

import android.arch.lifecycle.MutableLiveData
import android.view.View
import co.burcu.lootinterviewtask.R
import co.burcu.lootinterviewtask.base.BaseViewModel
import co.burcu.lootinterviewtask.model.User
import co.burcu.lootinterviewtask.network.UserApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Burcu Yalcinkaya on 23:01 8/29/18.
 */

class UserListViewModel : BaseViewModel() {

    @Inject
    lateinit var userApi: UserApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    var errorClickListener = View.OnClickListener { loadUsers() }
    val userListAdapter: UserListAdapter = UserListAdapter()

    init {
        loadUsers()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadUsers() {
        subscription = userApi.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveUserListStart() }
                .doOnTerminate { onRetrieveUserListFinish() }
                .subscribe(
                        { result -> onRetrieveUserListSuccess(result) },
                        { onRetrieveUserListError() }
                )
    }

    private fun onRetrieveUserListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveUserListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveUserListSuccess(userList: List<User>) {
        userListAdapter.updateUserList(userList)
    }

    private fun onRetrieveUserListError() {
        errorMessage.value = R.string.post_error
    }
}