package co.burcu.lootinterviewtask.ui.user

import android.arch.lifecycle.MutableLiveData
import co.burcu.lootinterviewtask.base.BaseViewModel
import co.burcu.lootinterviewtask.model.User

/**
 * Created by Burcu Yalcinkaya on 18:23 8/30/18.
 */

class UserViewModel : BaseViewModel() {
    private var userName = MutableLiveData<String>()
    private var userPhone = MutableLiveData<String>()
    private var userEmail = MutableLiveData<String>()

    fun bind(user: User) {
        userName.value = user.name
        userPhone.value = user.phone
        userEmail.value = user.email
    }

    fun getUserName(): MutableLiveData<String> {
        return userName
    }

    fun getUserPhone(): MutableLiveData<String> {
        return userPhone
    }

    fun getUserEmail(): MutableLiveData<String> {
        return userEmail
    }
}