package co.burcu.lootinterviewtask.injection.component

import co.burcu.lootinterviewtask.injection.module.NetworkModule
import co.burcu.lootinterviewtask.ui.user.UserListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Burcu Yalcinkaya on 23:04 8/29/18.
 */

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified UserListViewModel.
     * @param userListViewModel UserListViewModel in which to inject the dependencies
     */
    fun inject(userListViewModel: UserListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}