package co.burcu.lootinterviewtask.network


import co.burcu.lootinterviewtask.model.User
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Burcu Yalcinkaya on 8/29/18.
 */
interface UserApi {

    @GET("/users")
    fun getUsers(): Observable<List<User>>
}