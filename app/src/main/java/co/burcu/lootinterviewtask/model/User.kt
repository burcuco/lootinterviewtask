package co.burcu.lootinterviewtask.model

/**
 * Class which provides a model for user
 * @constructor Sets all properties of the user
 * @property name the name of the user
 * @property id the unique identifier of the user
 * @property phone the phone of the user
 * @property email the email of the user
 */
data class User(val id: Int, val name: String, val phone: String, val email: String)