package co.burcu.lootinterviewtask.base

import android.arch.lifecycle.ViewModel
import co.burcu.lootinterviewtask.injection.component.DaggerViewModelInjector
import co.burcu.lootinterviewtask.injection.component.ViewModelInjector
import co.burcu.lootinterviewtask.injection.module.NetworkModule
import co.burcu.lootinterviewtask.ui.user.UserListViewModel

/**
 * Created by Burcu Yalcinkaya on 8/29/18.
 */

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is UserListViewModel -> injector.inject(this)
        }
    }

}