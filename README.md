# README #

### What is this repository for? ###

* Loot Financial Service Interview Task
* 1.0

### Technologies ###

* Kotlin
* MVVM
* Retrofit
* Dagger2
* Rx Java
* Binding
* Android Lifecycle
* Android Support Libraries

### Contact Info ###
* me@burcu.co 
* burcuuturkmen@gmail.com

Thank you!